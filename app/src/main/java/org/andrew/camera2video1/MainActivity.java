package org.andrew.camera2video1;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity
{
    private final static String LOG_TAG = "Camera2Video1-MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button startButton = findViewById(R.id.button1);
        //createNotificationChannel();
        startButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                startForegroundService(new Intent(MainActivity.this, VideoRecordingService.class));
            }
        });
        Button stopButton = findViewById(R.id.button2);
        stopButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                stopService(new Intent(MainActivity.this, VideoRecordingService.class));
            }
        });
        Button showNotificationButton = findViewById(R.id.button3);
        showNotificationButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Log.i(LOG_TAG, "show notification button3: onClick");
                try
                {
                    // do stuff like register for BroadcastReceiver, etc.
                    Intent notificationIntent = new Intent(MainActivity.this, MainActivity.class);
                    notificationIntent.setAction("org.andrew.camera2video1.action.main");
                    notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this, 0,
                            notificationIntent, 0);

                    NotificationCompat.Builder notificationBuilder =
                            new NotificationCompat.Builder(MainActivity.this, "InvisibleVideoRecorder")
                                    .setSmallIcon(R.drawable.ic_directions_car_black)
                                    .setContentTitle("Hello world")
                                    .setContentText("Hello world")
                                    //.setTicker("TICKER")
                                    //.setOngoing(true)
                                    .setContentIntent(pendingIntent)
                                    ;

                    NotificationManager notificationManager =
                            (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

                    notificationManager.notify(001, notificationBuilder.build());
                    Log.i(LOG_TAG, "notify call ok");
                }
                catch (Exception e)
                {
                    Log.e(LOG_TAG, "show notification button3: onClick exception e = " + e.getMessage());
                }
            }
        });
    }
}
