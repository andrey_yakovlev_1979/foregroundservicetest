package org.andrew.camera2video1;

import android.Manifest;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Binder;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.PowerManager;
import android.provider.SyncStateContract;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Surface;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import static android.support.v4.app.NotificationCompat.PRIORITY_MIN;

public class VideoRecordingService extends Service
{
    private final static String LOG_TAG = "VideoRecordingService";
    private final static String TAG = "VideoRecordingService";

    InvisibleVideoRecorder recorder = null;

    public class MyBinder extends Binder
    {
        public VideoRecordingService getService()
        {
            return VideoRecordingService.this;
        }
    }

    private final IBinder binder = new MyBinder();

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        //return binder;
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        Log.i(LOG_TAG, "onStartCommand");
        recorder = new InvisibleVideoRecorder(this);
        recorder.start();
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy()
    {
        Log.i(LOG_TAG, "onDestroy");
        if (recorder != null)
        {
            Log.i(LOG_TAG, "onDestroy: calling recorder.stop");
            recorder.stop();
        }
        else
        {
            Log.i(LOG_TAG, "onDestroy: recorder is null, can't stop");
        }
    }
    @Override
    public void onCreate() {
        super.onCreate();

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Log.i(LOG_TAG, "onCreate: calling createNotificationChannel");
        createNotificationChannel(notificationManager);

        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setAction("org.andrew.camera2video1.action.main");
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        Notification.Builder notificationBuilder =
                new Notification.Builder(this, "InvisibleVideoRecorder")
                        .setSmallIcon(R.drawable.ic_launcher_background)
                        .setContentTitle("Идет запись...")
                        .setContentText("Идет запись...")
                        .setOngoing(true)
                        .setContentIntent(pendingIntent)
                ;

        Notification notification = notificationBuilder.build();
        startForeground(101, notification);
    }

    private String createNotificationChannel(NotificationManager notificationManager){
        String channelId = "InvisibleVideoRecorder";
        String channelName = "InvisibleVideoRecorder";
        NotificationChannel channel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);
        // omitted the LED color
        channel.setShowBadge(true);
        channel.setImportance(NotificationManager.IMPORTANCE_LOW);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        notificationManager.createNotificationChannel(channel);
        Log.i(LOG_TAG, "notification channel created, returning channelId = " + channelId);
        return channelId;
    }

    public class InvisibleVideoRecorder {
        private static final String TAG = "InvisibleVideoRecorder";
        private final CameraCaptureSessionStateCallback cameraCaptureSessionStateCallback = new CameraCaptureSessionStateCallback();
        private final CameraDeviceStateCallback cameraDeviceStateCallback = new CameraDeviceStateCallback();
        private MediaRecorder mediaRecorder;
        private CameraManager cameraManager;
        private Context context;

        private CameraDevice cameraDevice;

        private HandlerThread handlerThread;
        private Handler handler;

        PowerManager.WakeLock wl;

        public InvisibleVideoRecorder(Context context) {
            this.context = context;
            init();
        }

        private void init()
        {
            Log.i(TAG, "init");
            handlerThread = new HandlerThread("InvisibleVideoRecorderCamera");
            handlerThread.start();
            handler = new Handler(handlerThread.getLooper());

            try {
                mediaRecorder = new MediaRecorder();

                mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
                mediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);

                final String filename = context.getExternalFilesDir(Environment.DIRECTORY_MOVIES).getAbsolutePath() + File.separator + System.currentTimeMillis() + ".mp4";
                mediaRecorder.setOutputFile(filename);
                Log.i(TAG, "init: " + filename);

                CamcorderProfile profile = CamcorderProfile.get(CameraMetadata.LENS_FACING_BACK, CamcorderProfile.QUALITY_480P);
                mediaRecorder.setOrientationHint(0);
                mediaRecorder.setProfile(profile);
                mediaRecorder.prepare();
            } catch (IOException e) {
                Log.e(TAG, "init: exception" + e.getMessage());
            }
        }

        public void start() {
            Log.i(TAG, "start: ");

            wl = ((PowerManager)getSystemService(Context.POWER_SERVICE)).newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, ":InvisibleVideoRecorderWL");
            PowerManager pm;
            wl.acquire();

            cameraManager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
            try {
                cameraManager.openCamera(String.valueOf(CameraMetadata.LENS_FACING_BACK), cameraDeviceStateCallback, handler);
            } catch (CameraAccessException | SecurityException e) {
                Log.d(TAG, "start: exception " + e.getMessage());
            }

        }

        public void stop() {
            Log.d(TAG, "stop: ");
            mediaRecorder.stop();
            mediaRecorder.reset();
            mediaRecorder.release();
            cameraDevice.close();
            wl.release();
        }

        private class CameraCaptureSessionStateCallback extends CameraCaptureSession.StateCallback {

            @Override
            public void onActive(CameraCaptureSession session) {
                Log.d(TAG, "CameraCaptureSession.StateCallback onActive: ");
                super.onActive(session);
            }

            @Override
            public void onClosed(CameraCaptureSession session) {
                Log.d(TAG, "CameraCaptureSession.StateCallback onClosed: ");
                super.onClosed(session);
            }

            @Override
            public void onConfigured(CameraCaptureSession session) {
                Log.d(TAG, "CameraCaptureSession.StateCallback onConfigured: ");
            }

            @Override
            public void onConfigureFailed(CameraCaptureSession session) {
                Log.d(TAG, "CameraCaptureSession.StateCallback onConfigureFailed: ");
            }

            @Override
            public void onReady(CameraCaptureSession session) {
                Log.d(TAG, "CameraCaptureSession.StateCallback onReady: ");
                super.onReady(session);
                try {
                    CaptureRequest.Builder builder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
                    builder.addTarget(mediaRecorder.getSurface());
                    CaptureRequest request = builder.build();
                    session.setRepeatingRequest(request, null, handler);
                    mediaRecorder.start();
                } catch (CameraAccessException e) {
                    Log.d(TAG, "onConfigured: " + e.getMessage());

                }
            }

            @Override
            public void onSurfacePrepared(CameraCaptureSession session, Surface surface) {
                Log.d(TAG, "CameraCaptureSession.StateCallback onSurfacePrepared: ");
                super.onSurfacePrepared(session, surface);
            }
        }

        private class CameraDeviceStateCallback extends CameraDevice.StateCallback {
            private final static String TAG = "InvisibleVideoRecorder";

            @Override
            public void onClosed(CameraDevice camera) {
                Log.d(TAG, "CameraDevice.StateCallback onClosed: ");
                super.onClosed(camera);
            }

            @Override
            public void onDisconnected(CameraDevice camera) {
                Log.d(TAG, "CameraDevice.StateCallback onDisconnected: ");
            }

            @Override
            public void onError(CameraDevice camera, int error) {
                Log.e(TAG, "CameraDevice.StateCallback onError: error = " + error);
                stop();
            }

            @Override
            public void onOpened(CameraDevice camera) {
                Log.d(TAG, "CameraDevice.StateCallback onOpened: ");
                cameraDevice = camera;
                try {
                    camera.createCaptureSession(Arrays.asList(mediaRecorder.getSurface()), cameraCaptureSessionStateCallback, handler);
                } catch (CameraAccessException e) {
                    Log.d(TAG, "CameraDevice.StateCallback onOpened: " + e.getMessage());
                }
            }
        }

    }
}
